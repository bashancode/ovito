.. _development:

======================
Developer instructions
======================

The source code of :program:`OVITO` is available at https://gitlab.com/stuko/ovito/.
It serves as basis of the *OVITO Basic* program package distributed by OVITO GmbH, which
is made available under the GPLv3 and MIT open source licenses.

.. toctree::
   :maxdepth: 1

   requirements
   build_linux
   build_windows
   build_macosx
