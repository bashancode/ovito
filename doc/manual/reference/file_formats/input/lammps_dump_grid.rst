.. _file_formats.input.lammps_dump_grid:
  
LAMMPS dump grid file reader
----------------------------

For loading :ref:`voxel grid <scene_objects.voxel_grid>` data from files written by the *dump grid* command of the LAMMPS simulation code.

The file reader can decompress gzipped files (".gz" suffix) on the fly.