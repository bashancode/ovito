.. _about_ovito:

===========
About OVITO
===========

.. image:: images/ovito_screenshot.jpg
  :width: 60%
  :align: right

OVITO is a scientific data visualization and analysis software for molecular and other particle-based simulation models,
which are commonly used in computational materials science and engineering, physics, and chemistry disciplines. The software has served as a valuable
tool in over 9,500 research publications.

OVITO is a desktop application for Windows, Linux, and macOS. The software is being developed, distributed, and supported by `OVITO GmbH <https://www.ovito.org>`__,
a German startup company founded by the original developer of OVITO, `Dr. Alexander Stukowski <http://scholar.google.com/citations?user=f8Tw3eEAAAAJ>`__,
and other team members. The ecosystem includes the :ref:`OVITO Python module <scripting_manual>`, which is also available as a standalone product.

The `first public release of OVITO <http://stacks.iop.org/0965-0393/18/015012>`__ dates back to 2009, when the software had been created as a byproduct of a Ph.D.
research project at the Materials Science Department of Technische Universität Darmstadt in Germany. Over many years, OVITO remained an academic open-source project,
which was kept alive by voluntary efforts.

As an important step to secure the long-term availability of OVITO for the scientific community, Dr. Stukowski founded the company OVITO GmbH in 2020, together
with Dr. Constanze Kalcher. The entity's long-term mission is to secure software development, professional maintenance, and user support through
financial contributions (license fees) from OVITO users.

For more information about OVITO, please check out this user manual or visit our website `www.ovito.org <https://www.ovito.org>`__.